# Copyright 2022 Proyectos y Sistemas de Mantenimiento SL (eProsima).
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
HelloWorld Subscriber
"""
import signal
import matplotlib.pyplot as plt
import fastdds
import datetime as dt
import CartPoleFeedback
import numpy as np
from animaton import pole_cart_animate
import matplotlib.animation as animation

class Plotter:

    def __init__(self):
        pass


    def plot_state(data):
        plt.style.use('_mpl-gallery')

        # make data
        x = np.linspace(0, 10, len(data))
        print(data)
        y = 4 + 2 * np.sin(2 * x)

        # plot
        fig, ax = plt.subplots()

        ax.plot(x, data, linewidth=2.0)

        #ax.set(xlim=(0, 8), xticks=np.arange(1, 8),
        #       ylim=(-1, 1), yticks=np.arange(1, 8))

        plt.show()

    def plot_states(pos, d_pos, dd_pos, angle, d_angle, dd_angle, time, name="Plot"):
        fig, axs = plt.subplots(3, 2)
        fig.suptitle(name, fontsize=14)
        axs[0, 0].plot(time, pos)
        axs[0, 0].set_title("x")
        axs[1, 0].plot(time, d_pos)
        axs[1, 0].set_title("xd")
        axs[2, 0].plot(time, dd_pos)
        axs[2, 0].set_title("xdd")
        axs[0, 1].plot(time, angle)
        axs[0, 1].set_title("theta")
        axs[1, 1].plot(time, d_angle)
        axs[1, 1].set_title("theta d")
        axs[2, 1].plot(time, dd_angle)
        axs[2, 1].set_title("theta dd")
        fig.tight_layout()

    def show_plot(self):
        plt.show()



DESCRIPTION = """HelloWorld Subscriber example for Fast DDS python bindings"""
USAGE = ('python3 HelloWorldSubscriber.py')


# To capture ctrl+C
def signal_handler(sig, frame):
    print('Interrupted!')


class ReaderListener(fastdds.DataReaderListener):
    def __init__(self):
        self.data_store = []
        super().__init__()

    def on_subscription_matched(self, datareader, info):
        if (0 < info.current_count_change):
            print("Subscriber matched publisher {}".format(info.last_publication_handle))
        else:
            print("Subscriber unmatched publisher {}".format(info.last_publication_handle))

    def on_data_available(self, reader):
        info = fastdds.SampleInfo()
        data = CartPoleFeedback.CartPoleFeedback()
        reader.take_next_sample(data, info)
        self.data_store.append(data.cart_x())
        print("Received {message} : {index}".format(message=data.cart_x(), index=data.cart_x_dot()))
        #plotter.plot_states(data.cart_x(), data.cart_x_dot(), data.cart_x_dotdot(), data.pole_theta(),
                            #data.pole_theta_dot(), data.pole_theta_dotdot(), 1)

    def get_data(self):
        return self.data_store

class Reader:

    def __init__(self):
        self.plotter = Plotter

        ## Realtime plotter
        plt.rcParams['animation.html'] = 'html5'
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(1, 1, 1)
        self.xs = []
        self.ys = []


        factory = fastdds.DomainParticipantFactory.get_instance()
        self.participant_qos = fastdds.DomainParticipantQos()
        factory.get_default_participant_qos(self.participant_qos)
        self.participant = factory.create_participant(0, self.participant_qos)

        self.topic_data_type = CartPoleFeedback.CartPoleFeedbackPubSubType()
        self.topic_data_type.setName("CartPoleFeedback")
        self.type_support = fastdds.TypeSupport(self.topic_data_type)
        self.participant.register_type(self.type_support)

        self.topic_qos = fastdds.TopicQos()
        self.participant.get_default_topic_qos(self.topic_qos)
        self.topic = self.participant.create_topic("CartPoleFeedbackTopic", self.topic_data_type.getName(),
                                                   self.topic_qos)

        self.subscriber_qos = fastdds.SubscriberQos()
        self.participant.get_default_subscriber_qos(self.subscriber_qos)
        self.subscriber = self.participant.create_subscriber(self.subscriber_qos)

        self.listener = ReaderListener()
        self.reader_qos = fastdds.DataReaderQos()
        self.subscriber.get_default_datareader_qos(self.reader_qos)
        self.reader = self.subscriber.create_datareader(self.topic, self.reader_qos, self.listener)
        ani = animation.FuncAnimation(self.fig, self.animate, fargs=(self.xs, self.ys), interval=10)

        plt.show()

    def animate(self, i, xs, ys):
        # Read temperature (Celsius) from TMP102
        if(len(self.listener.data_store)>0):
            print(self.listener.data_store[-1])
            temp_c = round(self.listener.data_store[-1], 2)
        else:
            temp_c = 0;

        # Add x and y to lists
        xs.append(dt.datetime.now().strftime('%H:%M:%S.%f'))
        ys.append(temp_c)

        # Limit x and y lists to 20 items
        xs = xs[-2000:]
        ys = ys[-2000:]

        # Draw x and y lists
        self.ax.clear()
        self.ax.plot(xs, ys)
        self.ax.xaxis.set_major_locator(plt.MaxNLocator(10))

        # Format plot
        plt.xticks(rotation=45, ha='right')
        plt.subplots_adjust(bottom=0.30)
        plt.title('TMP102 Temperature over Time')
        plt.ylabel('Temperature (deg C)')

    def delete(self):

        factory = fastdds.DomainParticipantFactory.get_instance()
        self.participant.delete_contained_entities()
        factory.delete_participant(self.participant)

    def run(self):
        signal.signal(signal.SIGINT, signal_handler)
        print('Press Ctrl+C to stop')

        signal.pause()
        self.delete()

        self.plotter.plot_state(self.listener.data_store)


if __name__ == '__main__':
    print('Creating subscriber.')
    reader = Reader()
    reader.run()
    time = np.array([1,2,3])
    position = np.array([1,2,3])
    angle = np.array([1,2,3])

    # plotter.plot_states(x, dx, ddx, t, dt, ddt, time, "RK4 Simulation")
    #pole_cart_animate(angle, position, time)

    exit()
