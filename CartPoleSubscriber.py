# Copyright 2022 Proyectos y Sistemas de Mantenimiento SL (eProsima).
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
HelloWorld Subscriber
"""
import math
import signal
import matplotlib.pyplot as plt
import fastdds
import datetime as dt
import CartPoleFeedback
import numpy as np
from animaton import pole_cart_animate
import matplotlib.animation as animation


DESCRIPTION = """HelloWorld Subscriber example for Fast DDS python bindings"""
USAGE = ('python3 HelloWorldSubscriber.py')


# To capture ctrl+C
def signal_handler(sig, frame):
    print('Interrupted!')


class ReaderListener(fastdds.DataReaderListener):
    def __init__(self):
        self.data_store = []
        super().__init__()

    def on_subscription_matched(self, datareader, info):
        if (0 < info.current_count_change):
            print("Subscriber matched publisher {}".format(info.last_publication_handle))
        else:
            print("Subscriber unmatched publisher {}".format(info.last_publication_handle))

    def on_data_available(self, reader):
        info = fastdds.SampleInfo()
        data = CartPoleFeedback.CartPoleFeedback()
        reader.take_next_sample(data, info)
        self.data_store = [data.cart_x(), data.pole_theta()]
        #print("Received {message} : {index}".format(message=data.cart_x(), index=data.cart_x_dot()))
        #plotter.plot_states(data.cart_x(), data.cart_x_dot(), data.cart_x_dotdot(), data.pole_theta(),
                            #data.pole_theta_dot(), data.pole_theta_dotdot(), 1)

    def get_data(self):
        return self.data_store

class Reader:

    def __init__(self):


        ## Realtime plotter
        plt.rcParams['animation.html'] = 'html5'
        self.fig = plt.figure(figsize=(12.8, 6.4))
        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-2.4, 2.4), ylim=(-1.2, 1.2))
        ax.set_xlabel('position')
        ax.get_yaxis().set_visible(False)


        self.rod, = ax.plot([], [], 'o-', color='k', lw=4,
                       markersize=6, markeredgecolor='k',
                       markerfacecolor='k')
        self.cart, = ax.plot([], [], linestyle='None', marker='s', markersize=40, markeredgecolor='k', color='orange',
                        markeredgewidth=2)
        self.pendulum, = ax.plot([], [], linestyle='None', marker='o',
                                 markersize=20, markeredgecolor='k',
                                 color='orange', markeredgewidth=2)

        factory = fastdds.DomainParticipantFactory.get_instance()
        self.participant_qos = fastdds.DomainParticipantQos()
        factory.get_default_participant_qos(self.participant_qos)
        self.participant = factory.create_participant(0, self.participant_qos)

        self.topic_data_type = CartPoleFeedback.CartPoleFeedbackPubSubType()
        self.topic_data_type.setName("CartPoleFeedback")
        self.type_support = fastdds.TypeSupport(self.topic_data_type)
        self.participant.register_type(self.type_support)

        self.topic_qos = fastdds.TopicQos()
        self.participant.get_default_topic_qos(self.topic_qos)
        self.topic = self.participant.create_topic("CartPoleFeedbackTopic", self.topic_data_type.getName(),
                                                   self.topic_qos)

        self.subscriber_qos = fastdds.SubscriberQos()
        self.participant.get_default_subscriber_qos(self.subscriber_qos)
        self.subscriber = self.participant.create_subscriber(self.subscriber_qos)

        self.listener = ReaderListener()
        self.reader_qos = fastdds.DataReaderQos()
        self.subscriber.get_default_datareader_qos(self.reader_qos)
        self.reader = self.subscriber.create_datareader(self.topic, self.reader_qos, self.listener)
        ani = animation.FuncAnimation(self.fig, self.animate, interval=1)

        plt.show()

    def animate(self, i):
        if len(self.listener.data_store) < 2:
            return
        position = self.listener.data_store[0]
        angle = -self.listener.data_store[1]
        print(angle)
        pendulum_length = 1
        x1 = position
        #y1 = np.zeros(len(time))
        y1 = 0
        x2 = pendulum_length * np.sin(angle) + x1
        x2b = pendulum_length * 1.05 * math.sin(angle) + x1
        y2 = pendulum_length * math.cos(angle) - y1
        y2b = pendulum_length * 1.05 * math.cos(angle) - y1

        self.cart.set_data([x1], [y1 - 0.05])
        self.pendulum.set_data([x2b], [y2b])
        self.rod.set_data([x1, x2], [y1 + 0.01, y2])
        # Add x and y to lists

        plt.subplots_adjust(bottom=0.30)

    def delete(self):

        factory = fastdds.DomainParticipantFactory.get_instance()
        self.participant.delete_contained_entities()
        factory.delete_participant(self.participant)

    def run(self):
        signal.signal(signal.SIGINT, signal_handler)
        print('Press Ctrl+C to stop')

        #signal.pause()
        self.delete()



if __name__ == '__main__':
    print('Creating subscriber.')
    reader = Reader()
    reader.run()
    time = np.array([1,2,3])
    position = np.array([1,2,3])
    angle = np.array([1,2,3])

    # plotter.plot_states(x, dx, ddx, t, dt, ddt, time, "RK4 Simulation")
    #pole_cart_animate(angle, position, time)

    exit()
